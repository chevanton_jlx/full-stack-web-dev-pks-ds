//Soal 1

const box = (p, l) => ({
    luas: p*l,
    keliling: 2*(p+l)
});

console.log(box (10, 5));


//Soal 2

const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function(){
            console.log(firstName + " " + lastName)
        }
    }
}

//Driver Code---------------------------------?
// const obj = {
//     firstName,
//     lastName,
//     fullName
// }
newFunction("William", "Imoh").fullName() 


// Soal 3

const newObject = {firstName: "Muhammad", lastName: "Iqbal Mubarok", address: "Jalan Ranamanyar", hobby: "playing football",}; const {firstName, lastName, address, hobby} = newObject; console.log(firstName, lastName, address, hobby);


// Soal 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east) -------------------- SyntaxError: Identifier 'combined' has already been declared
const combined = [...west, ...east]   

console.log(combined)


// Soal 5

const planet = "earth" 
const view = "glass" 
const before = `Lorem ${view} dolor sit amet, + consectetur adipiscing elit, ${planet}`

console.log(before)