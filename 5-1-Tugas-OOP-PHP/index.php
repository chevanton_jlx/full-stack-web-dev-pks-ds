<?php

abstract class Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function __construct($nama, $darah, $jumlahKaki, $keahlian) {
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
    }
    
    abstract public function atraksi();
}

abstract class Fight {
    public $attackPower;
    public $defencePower;
    
    public function __construct($attackPower, $defencePower) {
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }
    
    abstract public function serang();
    
    abstract public function diserang();
}

class Elang extends Hewan {
    public function atraksi() {
        return $this->nama ." sedang terbang tinggi";
  }
}

class Harimau extends Hewan {
    public function atraksi() {
        return $this->nama ." sedang berlari cepat";
  }
}

$elang1 = new elang1("Elang");
echo $elang1->atraksi();
echo "<br>";

$harimau1 = new harimau1("Harimau");
echo $harimau1->atraksi();
echo "<br>";

?>