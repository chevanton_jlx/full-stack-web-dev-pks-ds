<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Verifikasi Registrasi</title>
</head>
<body>
    <h2>Selamat Datang di Web Laravel API</h2>
    Yang terhormat {{ $otp_code->user->name }}, akun Anda telah berhasil didaftarkan. Ini adalah kode OTP Anda:
    <br> {{ $otp_code->otp }} <br>
    Kode OTP ini berlaku 5 menit. Jangan berikan kode ini kepada siapapun.
</body>
</html>