<?php

namespace App\Listeners;

use App\Mail\RegenerateOtpMail;
use Illuminate\Support\Facades\Mail;
use App\Events\RegenerateOtpStoredEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailForNewOtp implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateOtpStoredEvent  $event
     * @return void
     */
    public function handle(RegenerateOtpStoredEvent $event)
    {
        //send email for New OTP Code to User
        Mail::to($event->otp_code->user->email)->send(new RegenerateOtpMail($event->otp_code));
    }
}
