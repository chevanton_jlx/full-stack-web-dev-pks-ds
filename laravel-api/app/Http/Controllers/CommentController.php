<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CommentStoredEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        return $this->middleware('auth:api')->except(['index' , 'show']);
    }

    public function index(Request $request)
    {
        //request post_id to make reference for show only comments on those post
        $post_id = $request->post_id;

        //get data from table comments
        $comments = Comment::where('post_id', $post_id)->latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Comments',
            'data'    => $comments
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comment = Comment::create([
            'content'     => $request->content,
            'post_id'   => $request->post_id
        ]);

        //memanggil event CommentSoredEvent
        event(new CommentStoredEvent($comment));


        //success save to database
        if($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $comment
            ], 201);

        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find post by ID
        $comment = Comment::findOrfail($id);

        if ($comment) {
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Comment',
                'data'    => $comment
            ], 200);
            
        }

        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment Not Found',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find comment by ID
        $comment = Comment::findOrFail($id);

        if($comment) {

            //check user to allow edit
            $user = auth()->user();

            if ($comment->user_id !=$user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'User is not allow to edit this comment'
                ], 403);
            }

            //update comment
            $comment->update([
                'content'     => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $comment
            ], 200);

        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find comment by ID
        $comment = Comment::findOrfail($id);

        if($comment) {

            //check user to allow delete
            $user = auth()->user();

            if ($comment->user_id !=$user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'User is not allow to delete this comment'
                ], 403);
            }

            //delete comment
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);

        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}
