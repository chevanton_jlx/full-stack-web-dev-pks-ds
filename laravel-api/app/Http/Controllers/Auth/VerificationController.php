<?php

namespace App\Http\Controllers\Auth;

use App\OtpCode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        //set validation
        $validator = Validator::make($request->all(), [
            'otp' => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //check otp is exist
        $otp_code = OtpCode::where('otp', $request->otp)->first();

        //if otp not found
        if (!$otp_code) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code not found!'
            ], 400);
        }

        //if otp was found, check validate time first
        $now = Carbon::now();

        //if otp code was expired
        if ($now > $otp_code->valid_until) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code was expired!'
            ], 400);
        }

        //if otp code is valid, update user
        $user = User::find($otp_code->user_id);
        $user->update([
            'email_verified_at' => now(),
        ]);

        //then, delete otp code was verified
        $otp_code->delete();

        return response()->json([
            'success' => true,
            'message' => 'User verification was successful',
            'data' => $user
        ], 400);
    }
}
