<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\RegisterStoredEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //make users database
        $user = User::create($allRequest);

        //check otp code is exist
        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        //for valid_until time
        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp'           => $random,
            'user_id'       => $user->id,
            'valid_until'   => $now->addMinutes(5)
        ]);

        //memanggil event RegisterSoredEvent
        event(new RegisterStoredEvent($otp_code));

        //success save to database
        return response()->json([
            'success' => true,
            'message' => 'User Created',
            'data'    => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ], 201);
        
    }
}
