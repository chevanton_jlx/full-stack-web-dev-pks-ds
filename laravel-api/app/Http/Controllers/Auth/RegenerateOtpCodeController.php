<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Events\RegenerateOtpStoredEvent;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        //set validation
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //check user is exist
        $user = User::where('email', $request->email)->first();

        //delete otp code was created
        if ($user->otp_code) {
            $user->otp_code->delete();
        }

        //check otp code is exist
        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        //for valid_until time
        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp'           => $random,
            'user_id'       => $user->id,
            'valid_until'   => $now->addMinutes(5)
        ]);

        //memanggil event RegenerateOtpStoredEvent
        event(new RegenerateOtpStoredEvent($otp_code));

        //success save to database
        return response()->json([
            'success' => true,
            'message' => 'OTP Code successfully re-generate',
            'data'    => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ], 201);

    }

}
